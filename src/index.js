import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider, connect} from 'react-redux';
import thunk from 'redux-thunk';
import './sass/index.scss';


// Define Action Creators
const updateNameStart = (name) => {
	return dispatch => {
		setTimeout(() => {
			dispatch(
				{type: 'update_name_finish', name}
			);
		}, 500);
	}
};

const updateNameFinish = () => ({
	type: 'update_name_finish'
});

// Define reducer + initial state
const initialState = {
	name: 'Friend!'
};

const reducer = (state = initialState, action) => {
	switch(action.type) {
		case 'update_name_start':
			return state;

		case 'update_name_finish':
			return {...state, name: action.name}

		default:
			return state;
	}
};

// Create a store and install thunk middleware.
const store = createStore(reducer, applyMiddleware(thunk));

// Define example component to test React + Redux
const TestComponent = (props) => (
	<div>
		<h1>Hello, {props.name}!</h1>
		<input
			type="text"
			placeholder="What's your name?"
			onChange={props.handleUpdateName} />
	</div>
);

// Wire Up Redux / Data binding
const mapStateToProps = (state) => ({
	name: state.name
});

const mapDispatchToProps = (dispatch) => ({
	handleUpdateName: (el) => dispatch(updateNameStart(el.target.value))
});

const TestComponentRedux = connect(mapStateToProps, mapDispatchToProps)(TestComponent);

// Create a root component to attach everything to.
const Root = () => (
	<Provider store={store}>
		<TestComponentRedux />
	</Provider>
);

// Demonstrate object spread operator.
const obj1 = {foo: 'bar'};
const obj2 = {...obj1, hello: 'you funky homosapien!'};
console.log(obj2);

// Mount + display our example.
var mountPoint = document.getElementById('mountPoint');
ReactDOM.render(<Root />, mountPoint);
