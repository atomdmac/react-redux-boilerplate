const path = require('path');

module.exports = {
	entry: './src/index.js',
	output: {
		filename: 'bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
	module: {
		loaders: [
			{ test: /\.js$/, loader: 'babel-loader', exclude: '/node_modules' },
			{ test: /\.scss$/, loaders: ['style-loader', 'css-loader', 'sass-loader'], exclude: '/node_modules' }
		]
	},
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		port: 8080
	}
};
